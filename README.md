Project Name
===============================
Assignment 

Version of React Native Used
===============================
React native Version : "0.61.5",
React : "16.9.0"

Installed Required packaged with version
===============================
 redux : "4.0.5",
 react-redux : "7.1.3",
 moment : "^2.24.0",
 redux-thunk: "2.3.0"
 prop-types: "15.7.2"   

Configuration instructions
===============================
Require below packaged to need to install in system
1) Node.js  
2) React native 

How to install and Run project 
===============================
Please follow the below step for command line 
1) Setup project folder and install npm
2) command "react-native start" for start metro server
3) Run project in Android using command "react-native run-android"
4) Run Project in iOS using command "react-native run-ios"

How to work 
===============================
1) Initial load current month data 
2) User can see next and previos month data by using left and right arrow
3) user can switch power and rate by click particular button.
4) User click single consumed data then it will show in popup. 
5) If data will not found then showing not found message.






