import React, {Component} from 'react';
import {Provider} from 'react-redux';
import store from '../store/store';
import ConsumptionActions from '../actions/consumeAction';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConsumptionActions />
      </Provider>
    );
  }
}
