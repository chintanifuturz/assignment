/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  ActivityIndicator,
  View,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Image,
  SafeAreaView,
  Alert,
} from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';
import styles from './style';

const appName = 'getBarry';
const next = 'next';
const prev = 'prev';
const unit = ' Kwh ';
const currency = ' Kr ';

class Consumption extends Component {
  //load initial data of current month
  componentDidMount = () => this.props.getData(this.props.currentDate);
  // Convert date formate to 'DD/MM YYYY'
  dateFormat = date =>
    moment(date)
      .utc()
      .format('DD/MM YYYY');
  // Convert Month name and year from given date
  monthName = date =>
    moment(date)
      .utc()
      .format('MMMM YYYY');
  // Display data in pop up
  dataPopUp = (date, charge) => {
    if (this.props.activeTab === 1) {
      Alert.alert(appName, 'consumed ' + charge + ' Kwh on ' + date);
    } else if (this.props.activeTab === 0) {
      Alert.alert(appName, 'consumed ' + charge + ' Kr charge on ' + date);
    }
  };
  // Dispaly sum of monthly unit in Kwh and currency
  sum = () => {
    return (
      <View style={styles.sumView}>
        <Text
          style={[
            styles.sumTextStyle,
            {color: this.props.activeTab === 1 ? '#FF4B9B' : '#EF7B40'},
          ]}>
          {this.props.activeTab === 1
            ? this.props.totalEnergy
            : this.props.totalAmount}
          <Text style={styles.sumUnitTextStyle}>
            {this.props.activeTab === 1 ? unit : currency}
          </Text>
        </Text>
      </View>
    );
  };

  // Call this view when data will empty
  emptyList = () => {
    return (
      <View style={styles.emptyListView}>
        <Text style={styles.emptyTextStyle}>{this.props.error}</Text>
      </View>
    );
  };
  // This is top section view to display month name and buttons
  topSection = () => {
    return (
      <View style={styles.topView}>
        <View style={styles.titleView}>
          <Text style={styles.titleText}> Your consumption</Text>
        </View>

        <View style={styles.topMiddleView}>
          <View style={styles.leftView}>
            <TouchableOpacity
              style={styles.arrow}
              onPress={() => this.props.getData(this.props.currentDate, prev)}>
              <Image
                source={require('../assets/images/left.png')}
                style={styles.arrow}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>

          <View style={styles.currentMonthView}>
            <Text style={styles.monthText}>
              {this.monthName(this.props.currentDate)}
            </Text>
          </View>

          <View style={styles.rightView}>
            <TouchableOpacity
              style={styles.arrow}
              onPress={() => this.props.getData(this.props.currentDate, next)}>
              <Image
                source={require('../assets/images/right.png')}
                style={styles.arrow}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.tabView}>
          <View style={styles.buttonView}>
            <TouchableOpacity
              style={[
                styles.tabClick,
                {
                  backgroundColor:
                    this.props.activeTab === 1 ? '#FF4B9B' : '#32363D',
                },
              ]}
              onPress={() => this.props.tab(1)}>
              <Text style={styles.buttonText}>kWh</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabClick,
                {
                  backgroundColor:
                    this.props.activeTab === 0 ? '#EF7B40' : '#32363D',
                },
              ]}
              onPress={() => this.props.tab(0)}>
              <Text style={styles.buttonText}>kr.</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  // this section will get and display data in flatlist
  bottomSection = () => {
    return (
      <View style={styles.bottomView}>
        <View style={styles.listDataView}>
          <FlatList
            ListHeaderComponent={this.sum}
            data={this.props.data.result}
            refreshing={this.props.isLoading}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.activeTab === 1
                    ? this.dataPopUp(
                        this.dateFormat(item.date),
                        item.value.toFixed(2),
                      )
                    : this.dataPopUp(
                        this.dateFormat(item.date),
                        item.priceIncludingVat.toFixed(2),
                      )
                }
                style={styles.singleDataView}>
                <View style={styles.date}>
                  <Text style={styles.dateText}>
                    {this.dateFormat(item.date)}
                  </Text>
                </View>
                <View style={styles.unitView}>
                  <Text style={styles.valueTextStyle}>
                    {this.props.activeTab === 1
                      ? item.value.toFixed(2)
                      : item.priceIncludingVat.toFixed(2)}
                    <Text style={styles.unitTextStyle}>
                      {this.props.activeTab === 1 ? unit : currency}
                    </Text>
                    <Image
                      source={require('../assets/images/unitarrow.png')}
                      style={styles.unitArrow}
                      resizeMode="contain"
                    />
                  </Text>
                </View>
              </TouchableOpacity>
            )}
            ListEmptyComponent={this.emptyList}
          />
        </View>
      </View>
    );
  };

  // Render Design of main screen
  render = () => {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="#2C2F36" barStyle="light-content" />

        <View style={styles.loaderView}>
          <ActivityIndicator animating={this.props.isLoading} size="large" />
        </View>

        {this.topSection()}

        {this.bottomSection()}
      </SafeAreaView>
    );
  };
}

// Validate Prop value  with proptype
Consumption.propTypes = {
  totalAmount: PropTypes.string.isRequired,
  activeTab: PropTypes.number.isRequired,
  isLoading: PropTypes.bool.isRequired,
  totalEnergy: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
  currrentDate: PropTypes.instanceOf(Date),
};

export default Consumption;
