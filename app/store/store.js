import {combineReducers, applyMiddleware, createStore, compose} from 'redux';
import thunk from 'redux-thunk';
import consumeReducer from '../reducers/consumeReducer';

//Combine Reducer
const AppReducers = combineReducers({
  consumeReducer,
});

const rootReducer = (state, action) => {
  return AppReducers(state, action);
};

//Creatre store
let store = createStore(rootReducer, compose(applyMiddleware(thunk)));

export default store;
